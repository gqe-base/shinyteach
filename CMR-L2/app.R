#
# This is a Shiny web application. You can run the application by clicking
# the 'Run App' button above.
#
# Find out more about building applications with Shiny here:
#
#    http://shiny.rstudio.com/
#


library(shiny)

# Define UI for application that draws a histogram
ui <- fluidPage(
   
   # Application title
   titlePanel("Méthode CMR : étape de recapture"),
   
   # Sidebar with a slider input for number of bins 
   sidebarLayout(
      sidebarPanel(
       
         numericInput("n1",
                     "Nombre d'individus recapturés",
                      value = 10
      
      ),
      actionButton("go", "Recapturer")
      ),
      
      # Show a plot of the generated distribution
      mainPanel(
         plotOutput("distPlot"),
         plotOutput("distPlot2")
      )
   )
)

# Define server logic required to draw a histogram
server <- function(input, output) {
   
   output$distPlot <- renderPlot({
      # calculate stat de test based on input$n from ui.R
     
      plot(runif(700,min=0,max=10),runif(700,min=0,max=10),ylim=c(-0.3,12),xlim=c(-0.3,10.3),axes=F,xlab="",ylab="", main= "Population")
      points(runif(300,min=0,max=10),runif(300,min=0,max=10),pch=19)
      legend("top",c("individus non marqués","individus marqués"),pch=c(1,19),horiz=T)
      box()
   })
      
   output$distPlot2 <- renderPlot({  
      
      n1=as.numeric(input$n1)
      pop=c(rep(19,300),rep(1,700))
     
      echan <- eventReactive(input$go, {
        sample(pop,n1)
      })
      echan=echan()
      
      echant=table(factor(echan,levels=unique(pop))) 

      plot(3,3,ylim=c(-0.3,4),xlim=c(-0.3,3.3),axes=F,xlab="",ylab="", main= paste("Recapture de",n1,"individus",sep=" "),type="n")
      for(i in 1:length(names(echant)))
      {
        points(runif(echant[i],min=0,max=3),runif(echant[i],min=0,max=3),pch=as.numeric(names(echant)[i]))
      }
      legend("top",legend=c(paste(echant[which(names(echant)==19)],"marqués",sep=" "),paste(echant[which(names(echant)==1)],"non-marqués",sep=" ")),pch=as.vector(as.numeric(names(echant))),horiz=T,title="Individus recapturés")
      box()
     
      

      
   
   })
   
   ### renderUi pur changer les parametres en fonction des parametres
   # reactive qui recalcule
}

# Run the application 
shinyApp(ui = ui, server = server)



